production:
	ELK_VERSION=7.4.0 docker-compose -f docker-compose.prod.yml up -d --build
	docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
	sleep 5
	docker-compose -f docker-compose.prod.yml exec web python manage.py install_labels

prod-down:
	ELK_VERSION=7.4.0 docker-compose -f docker-compose.prod.yml down -v

sudo-production:
	sudo ELK_VERSION=7.4.0 docker-compose -f docker-compose.prod.yml up -d --build
	sudo docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
	sleep 5
	sudo docker-compose -f docker-compose.prod.yml exec web python manage.py install_labels

sudo-prod-down:
	sudo ELK_VERSION=7.4.0 docker-compose -f docker-compose.prod.yml down -v

sudo-prod-tail:
	sudo docker-compose -f docker-compose.prod.yml logs -f web celery

development:
	ELK_VERSION=7.4.0 docker-compose up -d --build
	docker-compose exec web python manage.py migrate
	sleep 5
	docker-compose exec web python manage.py install_labels

dev-down:
	ELK_VERSION=7.4.0 docker-compose down -v

dev-tail:
	docker-compose logs -f web celery

dev-test:
	docker-compose exec web python3 ./manage.py test api_v1/tests/test_processor_factory.py

neo4j-dump:
	docker-compose exec neo4j neo4j stop
	docker-compose exec neo4j neo4j-admin dump --database=graph.db --to=/tmp/dump/`date +%Y%m%d`.dump
	docker-compose exec neo4j neo4j start

sudo-neo4j-dump:
	sudo docker-compose stop celery
	sudo docker-compose exec neo4j neo4j stop
	sudo docker-compose exec neo4j neo4j-admin dump --database=graph.db --to=/tmp/dump/`date +%Y%m%d`.dump
	sudo docker-compose exec neo4j neo4j start
	sudo docker-compose start celery

neo4j-load:
	docker-compose stop celery
	docker-compose exec neo4j neo4j stop
	docker-compose exec neo4j neo4j-admin load --from=/tmp/dump/`date +%Y%m%d`.dump --database=graph.db --force
	docker-compose exec neo4j neo4j start
	docker-compose start celery


sudo-neo4j-load:
	sudo docker-compose stop celery
	sudo docker-compose exec neo4j neo4j stop
	sudo docker-compose exec neo4j neo4j-admin load --from=/tmp/dump/`date +%Y%m%d`.dump --database=graph.db --force
	sudo docker-compose exec neo4j neo4j start
	sudo docker-compose start celery

neo4j-load-specific:
	docker-compose stop celery
	docker-compose exec neo4j neo4j stop
	docker-compose exec neo4j neo4j-admin load --from=/tmp/dump/$(filename) --database=graph.db --force
	docker-compose exec neo4j neo4j start
	docker-compose start celery

sudo-neo4j-load-specific:
	sudo docker-compose stop celery
	sudo docker-compose exec neo4j neo4j stop
	sudo docker-compose exec neo4j neo4j-admin load --from=/tmp/dump/$(filename) --database=graph.db --force
	sudo docker-compose exec neo4j neo4j start
	sudo docker-compose start celery

prod-neo4j-dump:
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin dump --database=graph.db --to=/tmp/dump/`date +%Y%m%d`.dump
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j start

sudo-prod-neo4j-dump:
	sudo docker-compose -f docker-compose.prod.yml stop celery
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin dump --database=graph.db --to=/tmp/dump/`date +%Y%m%d`.dump
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j start
	sudo docker-compose -f docker-compose.prod.yml start celery

prod-neo4j-load:
	docker-compose -f docker-compose.prod.yml stop celery
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin load --from=/tmp/dump/`date +%Y%m%d`.dump --database=graph.db --force
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j start
	docker-compose -f docker-compose.prod.yml start celery


sudo-prod-neo4j-load:
	sudo docker-compose -f docker-compose.prod.yml stop celery
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin load --from=/tmp/dump/`date +%Y%m%d`.dump --database=graph.db --force
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j start
	sudo docker-compose -f docker-compose.prod.yml start celery

prod-neo4j-load-specific:
	docker-compose -f docker-compose.prod.yml stop celery
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin load --from=/tmp/dump/$(filename) --database=graph.db --force
	docker-compose -f docker-compose.prod.yml exec neo4j neo4j start
	docker-compose -f docker-compose.prod.yml start celery

sudo-prod-neo4j-load-specific:
	sudo docker-compose -f docker-compose.prod.yml stop celery
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j stop
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j-admin load --from=/tmp/dump/$(filename) --database=graph.db --force
	sudo docker-compose -f docker-compose.prod.yml exec neo4j neo4j start
	sudo docker-compose -f docker-compose.prod.yml start celery

pearson-similarity:
	docker-compose exec neo4j python manage.py runscript pearson_similarity

cosine-similarity:
	docker-compose exec neo4j python manage.py runscript cosine_similarity

jaccard-similarity:
	docker-compose exec neo4j python manage.py runscript jaccard_similarity

nearest-similarity:
	docker-compose exec neo4j python manage.py runscript nearest_similarity

prod-pearson-similarity:
	dockercompose -f docker-compose.prod.yml python manage.py runscript pearson_similarity
