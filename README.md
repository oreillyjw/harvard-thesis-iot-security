https://realpython.com/test-driven-development-of-a-django-restful-api/

Allow `django` user to create new DB for testing
api=# ALTER USER django WITH CREATEDB;

commands:
```
python3 ./manage.py runserver
python3 ./manage.py makemigrations
python3 ./manage.py migrate
python3 ./manage.py test

python3 manage.py test --nocapture
python3 ./manage.py test api_v1/tests/test_processor_factory.py
Neo4j
python3 manage.py install_labels
Postgres
docker-compose exec db psql --username=django --dbname=thesis_api
```

```
python manage.py runscript process_file
python manage.py runscript pearson_similarity
python manage.py runscript cosine_similarity
```


super user:s

```
user: admin
passwd: <password>
```
CELERY
```
celery -A thesis_api worker -l info
````

Elasticsearch:
```
curl -XGET --user elastic:changeme localhost:9200/
```

Neo4j
http://localhost:7474/browser/


https://api.bgpview.io/ip/<IP>

Testing:
https://stackoverflow.com/questions/21500354/how-to-see-which-tests-were-run-during-djangos-manage-py-test-command

100% dockerize:
https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/#conclusion
https://nickjanetakis.com/blog/dockerize-a-flask-celery-and-redis-application-with-docker-compose


Cypher Queries
MATCH (n:DNSDestination {dns_name: "gateway-carry.icloud.com" })-[]-() RETURN n
MATCH (n:IPDestination) RETURN distinct n.ip, count(*)
MATCH (n) DETACH DELETE n
MATCH (n) RETURN n
-----------------
TODO:
* Algorithms
* POC of topology



https://learning.oreilly.com/library/view/machine-learning-and/9781491979891/
https://nickjanetakis.com/blog/dockerize-a-flask-celery-and-redis-application-with-docker-compose

Logging:
https://lincolnloop.com/blog/django-logging-right-way/


```
docker-compose -f docker-compose.prod.yml up -d --build
docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear
```




web_1            | 2019-11-18 01:55:03,778 api_v1.views ERROR    application/octet-stream
web_1            | 2019-11-18 01:55:03,782 django.request WARNING  Bad Request: /v3/unums/4e:ce:37:e8:86:34/router_configs/raw
