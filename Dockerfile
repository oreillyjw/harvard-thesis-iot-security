# pull official base image
FROM python:3.6.4-slim-jessie

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /usr/src/app
# set work directory
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y \
        python-dev python-pip python-setuptools \
        libffi-dev libxml2-dev libxslt1-dev \
        libtiff-dev libjpeg-dev zlib1g-dev libfreetype6-dev \
        liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev python-tk

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY ./resources/pythonwebserver/entrypoint.sh /usr/src/entrypoint.sh

COPY ./resources/pythonwebserver/entrypoint.sh /usr/src/entrypoint.prod.sh

# copy project
COPY thesis_api /usr/src/app/

# run entrypoint.sh
ENTRYPOINT ["/usr/src/entrypoint.sh"]
