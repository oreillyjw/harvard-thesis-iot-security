from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        r'^v3/unums/(?P<mac_address>([0-9a-fA-F]:?){12})/activate',
        views.put_unum_activate,
        name='put_unum_activate'
    ),
    url(
        r'^v3/unums/(?P<mac_address>([0-9a-fA-F]:?){12})/commands/(?P<uri_info>.*.+)?',
        views.get_unum_command_request,
        name='get_unum_command_request'
    ),
    url(
        r'^v3/unums/(?P<mac_address>([0-9a-fA-F]:?){12})/(?P<uri_info>.*.+)?',
        views.store_unum_request,
        name='store_unum_request'
    ),
    url(
        r'^v3/unums/$',
        views.get_all_unum_request,
        name='get_all_unum_request'
    )
]
