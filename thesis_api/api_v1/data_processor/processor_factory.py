from .processor import Processor
from .unum_processor import *

class ProcessorFactory:
    def __init__(self):
        self._creators = {
            "unum": UnumProcessor
        }

    def register_processor(self, data_type, creator):
        self._creators[data_type] = creator

    def get_processor(self, format=None, **kwargs):
        creator = self._creators.get(format)
        if not creator:
            creator = Processor
        return creator(**kwargs)
