import socket

class Processor:
    def __init__(self, **kwargs):
        self.source_type = kwargs.get("source_type", "unknown")
        self.data = kwargs.get("data", {})
        self.mac_address = kwargs.get("mac_address", None)
        self.data_type = kwargs.get("data_type", None)
        self.add_new_devices = kwargs.get("add_new_devices", False)

    @property
    def source_type(self):
        return self.__source_type

    @source_type.setter
    def source_type(self, source_type):
        self.__source_type = source_type

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        self.__data = data

    @property
    def mac_address(self):
        return self.__mac_address

    @mac_address.setter
    def mac_address(self, mac_address):
        self.__mac_address = mac_address

    @property
    def data_type(self):
        return self.__data_type

    @data_type.setter
    def data_type(self, data_type):
        self.__data_type = data_type

    @staticmethod
    def protocol_table():
        table = {num:name[8:] for name,num in vars(socket).items() if name.startswith("IPPROTO")}
        return table

    @classmethod
    def transfer_protocol_name(self,number):
        try:
            name = self.protocol_table()[number].lower()
        except:
            name = "unknown"
        return name

    def __str__(self):
        return(f"Type GENERAL PROCESSOR: {self.source_type}")

    def flatten(self):
        return self.data
