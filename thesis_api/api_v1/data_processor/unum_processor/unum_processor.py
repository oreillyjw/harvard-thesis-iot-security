from ..processor import Processor
from celery.utils.log import get_task_logger
logger = get_task_logger(__name__)
from copy import copy
from api_v1.models import UnumDevices, UnumPendingTelemetryFuncs
from django.core.exceptions import ObjectDoesNotExist
import re


class UnumProcessor(Processor):

    def __init__(self, **kwargs):
        Processor.__init__(self, **kwargs)

    def __str__(self):
        return(f"I am of type UNUM PROCESSOR: {self.source_type}")

    def flatten(self):
        flat_data = []

        if not self.able_to_flatten():
            return flat_data

        cur_dns = {}
        cur_devices= {}
        if 'dns' in self.data.keys():
            cur_dns = dict((dns_record["ip"], dns_record["name"]) for dns_record in self.data['dns'])

        try:
            if 'devices' in self.data.keys():
                for device in self.data['devices']:
                    current_device = {
                        "local_ip" : device["ip"],
                        "local_mac": device["mac"],
                        "router_mac_address": self.mac_address,
                        "data_type" : self.data_type,
                        "source_type" : self.source_type
                    }
                    if self.add_new_devices and not ( f'{device["mac"]}-device["ip"]' in cur_devices.keys()):
                        self.add_new_device(device)
                        cur_devices[f'{device["mac"]}-{device["ip"]}'] = 1
                    # check to see if device has been registered
                    for connections in device['conn']:
                        record = current_device
                        record['remote_ip'] = connections['r_ip']
                        record['remote_dns'] = cur_dns.get(connections['r_ip'],None)
                        record['remote_port'] = connections.get('r_port',None)
                        record['transfer_protocol_number'] = connections['proto']
                        record['transfer_protocol'] = self.transfer_protocol_name(connections['proto'])
                        record['b_in'] = sum(connections['b_in'])
                        record['b_out'] = sum(connections['b_out'])
                        flat_data.append(copy(record))
        except Exception as e:
            logger.error('Error at %s', exc_info=e)
            logger.error(f"flatten Exception of data: {self.data}")
        return flat_data

    def process_ssdp_data(self):
        data = []
        for ssdp in self.data['fingerprint']['ssdp']:
            record = {}
            regex = r"(LOCATION):\s+(.+?)\r"
            matches = re.finditer(regex, ssdp['info'], re.MULTILINE)
            ssdp_location = None
            for matchNum, match in enumerate(matches, start=1):
                ssdp_location = match.group(2)
            record['name'] = ssdp_location
            record['mac_address'] = ssdp['mac']
            record['router_mac_address'] = self.mac_address
            record['type'] = "ssdp"
            record['blob'] = ""
            record['port'] = 0
            data.append(record)
        if len(data) > 0:
            self.add_new_telemetry_func("fetch_urls")
        return data

    def process_mdns_data(self):
        data = []
        for mdns in self.data['fingerprint']['mdns']:
            record = mdns
            record['mac_address'] = mdns['mac']
            record['router_mac_address'] = self.mac_address
            record['type'] = "mdns"
            data.append(record)
        return data

    def flatten_fingerprints(self):
        flat_data = []

        if not self.able_to_flatten():
            return flat_data

        switcher = {
            "ssdp" : "process_ssdp_data",
            "mdns" : "process_mdns_data"
        }
        try:
            if 'fingerprint' in self.data.keys():
                for fingerprint in self.data['fingerprint']:
                    method_name = switcher[fingerprint] if fingerprint in switcher.keys() else "default_switcher"
                    if method_name != "default_switcher":
                        method = getattr(self,method_name,lambda : "default_switcher")
                        flat_data = flat_data + method()
        except Exception as e:
            logger.error('Error at %s', exc_info=e)
            logger.error(f"fingerprint Exception of data: {self.data}")

        return flat_data

    def able_to_flatten(self):
        if isinstance(self.data, str):
            return False

        return True

    def add_new_device(self, device):
        try:
            new_device = UnumDevices.objects.get(
                router_mac_address = self.mac_address,
                ip = device["ip"],
                mac_address=device["mac"]
            )
        except ObjectDoesNotExist:
            new_device = UnumDevices.objects.create(
                router_mac_address = self.mac_address,
                ip = device["ip"],
                mac_address=device["mac"]
            )
            self.add_new_telemetry_func("do_ssdp_discovery")
            self.add_new_telemetry_func("do_mdns_discovery")
            logger.error("ADDING NEW DEVICE")
        return new_device

    def add_new_telemetry_func(self,function):
        UnumPendingTelemetryFuncs.objects.create(
            mac_address=self.mac_address,
            processed=False,
            function=function
        )
