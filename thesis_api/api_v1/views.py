from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import UnumRequest, UnumPendingTelemetryFuncs, UnumDevicesMeta
from .serializers import *
from django.db.models import Count
from rest_framework.exceptions import ParseError, UnsupportedMediaType
from .tasks import elasticsearch_load, neo4j_provenance_load
import logging
from django.core.exceptions import ObjectDoesNotExist
import hashlib
from django.conf import settings
from rest_framework import status

# Get an instance of a logger
logger = logging.getLogger(__name__)

@api_view(['POST', 'PUT'])
def store_unum_request(request, mac_address, uri_info):
    # get details of a single mac address
    if ((request.method == 'POST' and
            ( request.content_type == "application/json"
                or request.content_type == "text/plain"
                or request.content_type == "application/octet-stream" ) )
            or
            ( request.method == 'PUT' and
                (request.content_type == "application/json"
                    or request.content_type == "text/plain"
                    or request.content_type == "application/octet-stream") ) ):
        data = {}
        if request.content_type == "application/json":
            data['data'] = request.data
            if data == "\"-\"":
                data = {}
        else:
            data['data'] =  request.body.decode('utf-8')


        request_info = UnumRequest.objects.create(
            request_verb=request.method.upper(),
            uri=request.get_full_path(),
            data=str(data),
            mac_address=mac_address
        )

        data['mac_address'] = mac_address
        data['data_type'] = uri_info
        data['source_type'] = "unum"
        elasticsearch_load.delay(data)
        neo4j_provenance_load.delay(data)
        if request.content_type == "application/json":
            logger.info(f"Telemetry Sequence : {uri_info}")
            if uri_info == "telemetry":
                telemetry_functions = process_telemetry(data,mac_address)
                return Response({"cmds":telemetry_functions})

        return Response()

    logger.error("UNABLE TO PROCESS REQUEST")
    logger.error(request.content_type)
    return Response()

def process_telemetry(data, mac_address):
    telemetry_functions  = []
    if data['data']["seq_num"] == 0:
        telemetry_functions = [ "blacklist" ]
    elif data['data']["seq_num"] == 14:
        telemetry_functions = [ "fetch_urls" ]
    else:
        # do_ssdp_discovery - do_mdns_discovery seem to occur every 12 hours and when a new device is triggered
        pending = UnumPendingTelemetryFuncs.objects.filter(mac_address=mac_address,processed=False)
        if len(pending) > 0:
            for e in pending:
                telemetry_functions.append(e.function)
                e.processed = True
                e.save()
        else:
            try:
                last_run = UnumPendingTelemetryFuncs.objects.filter(
                                mac_address=mac_address,
                                function="do_ssdp_discovery",
                                processed=True
                            ).latest('updated_at')
                if last_run.get_from_now() > 41760:
                    add_new_telemetry_func(mac_address, "do_ssdp_discovery")
                    # add_new_telemetry_func(mac_address, "do_mdns_discovery")
                    add_new_telemetry_func(mac_address, "fetch_urls")
            except:
                logger.info("No functions found")
    return telemetry_functions

def add_new_telemetry_func(mac_address, function):
    UnumPendingTelemetryFuncs.objects.create(
        mac_address=mac_address,
        processed=False,
        function=function
    )

@api_view(['GET'])
def get_unum_command_request(request, mac_address, uri_info):
    blacklist_ip = settings.API_IP
    response_info = {}
    status_code = status.HTTP_200_OK
    if uri_info == "blacklist":
        blacklist_cmd = f"iptables -t filter -D FORWARD -j unum_forward\niptables -F unum_forward\niptables -X unum_forward\niptables -N unum_forward\niptables -t filter -I FORWARD -j unum_forward\niptables -A unum_forward -d {blacklist_ip} -j ACCEPT\niptables -A unum_forward -s {blacklist_ip} -j ACCEPT\n\n\n\n"
        md5 = hashlib.md5(blacklist_cmd.encode('utf-8')).hexdigest()
        response_info = {"etag" : md5, "command": blacklist_cmd}
    elif uri_info == "do_mdns_discovery":
        response_info = ["._device-info._tcp.local", "._spotify-connect._tcp.local", "._printer._tcp.local", "._ipp._tcp.local", "._ipps._tcp.local", "._homekit._tcp.local", "._raop._tcp.local", "._nvstream._tcp.local"]
    elif uri_info == "fetch_urls":
        device_meta = UnumDevicesMeta.objects.filter(
            router_mac_address=mac_address,
            type="ssdp"
        )
        for meta_data in device_meta:
            logger.info(meta_data)
            response_info[meta_data.md5] = {"url" : meta_data.name}
    else:
        status_code = status.HTTP_404_NOT_FOUND
        response_info = { "error" : "This resource does not exist." }
    return Response(response_info, status=status_code)

@api_view(['PUT'])
def put_unum_activate(request, mac_address):
    return Response({"opmode" : "gw"})


@api_view(['GET'])
def get_all_unum_request(request):
    # get all mac_address
    if request.method == 'GET':
        instance = UnumRequest.objects.values('mac_address').annotate(the_count=Count('mac_address'))
        serializer = UnumMacAddressDictSerializer(instance, many=True)
        return Response(serializer.data)
