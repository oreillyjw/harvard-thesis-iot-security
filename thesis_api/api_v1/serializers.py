from rest_framework import serializers
from .models import UnumRequest

class UnumAllSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnumRequest
        fields = ('type', 'uri', 'mac_address', 'data', 'request_verb', 'created_at', 'updated_at')

class UnumMacAddressDictSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        return {
            'mac_address': obj['mac_address'],
            'count' : obj['the_count']
        }
