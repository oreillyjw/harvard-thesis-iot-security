from celery import shared_task
from celery.utils.log import get_task_logger
from .models import ElasticsearchBackend
from .models import UnumDevices, UnumDevicesMeta
from .models import RequestRel, IPDestination, DNSDestination, IOTDevice, GatewayDevice, NodeGetCreator
from .data_processor import ProcessorFactory
import hashlib
from bencode import bencode
from django.core.exceptions import ObjectDoesNotExist

logger = get_task_logger(__name__)

@shared_task
def elasticsearch_load(data):
    """Load data into elasticsearch"""
    logger.info(f"Start Loading data to elasticsearch")
    es = ElasticsearchBackend()
    factory = ProcessorFactory()
    processor = factory.get_processor(data["source_type"], **data)
    flat_data = processor.flatten()
    if len(flat_data) > 0:
        for record_data in flat_data:
            es.set(record_data)

    # fingerprint will only be done in elasticsearch since this is light processing
    fingerprint_data = processor.flatten_fingerprints()
    for record in fingerprint_data:
        data_md5 = hashlib.md5(bencode(record).encode('utf-8')).hexdigest()
        record["md5"] = data_md5
        logger.info(record)
        factory
        unum_device = UnumDevices.objects.get(
            router_mac_address = record['router_mac_address'],
            mac_address=record["mac_address"]
        )
        try:
            device_meta = UnumDevicesMeta.objects.get(
                device=unum_device,
                md5=record["md5"]
            )
        except ObjectDoesNotExist:
            device_meta = UnumDevicesMeta.objects.create(
                device=unum_device,
                router_mac_address=record['router_mac_address'],
                mac_address=record['mac_address'],
                md5=record["md5"],
                type=record['type'],
                blob=record['blob'],
                name=record['name'],
                port=record['port']
            )

        logger.info(f"{record}")


@shared_task
def neo4j_provenance_load(data):
    """Load data into Neo4j"""
    if data['source_type'] == "unum":
        factory = ProcessorFactory()
        exclude_keys = ['devices', 'dns']
        data['add_new_devices'] = True
        processor = factory.get_processor(data["source_type"], **data)
        flat_data = processor.flatten()
        if len(flat_data) > 0:
            new_d = {k: data['data'][k] for k in set(list(data['data'].keys())) - set(exclude_keys)}
            if 'data_type' in data.keys():
                logger.info(f"Processing DATA:{data['data_type']} :for upload:{new_d}")
            else:
                logger.info(f"Processing DATA for upload:{data}")
            for record_data in flat_data:
                rel_data, destination_ip_data, destination_dns_data, iot_data, gateway_data = NodeGetCreator.unum_data_transform(record_data)
                destination_ip_device = NodeGetCreator.get_create(IPDestination, destination_ip_data)
                gateway = NodeGetCreator.get_create(GatewayDevice, gateway_data)
                device = NodeGetCreator.get_create(IOTDevice, iot_data)
                if "dns_name" in destination_dns_data.keys():
                    destination_dns_device = NodeGetCreator.get_create(DNSDestination, destination_dns_data)
                    if not destination_ip_device.dns_names.is_connected(destination_dns_device):
                        destination_ip_device.dns_names.connect(destination_dns_device)

                if not gateway.iot_device.is_connected(device):
                    gate_rel = gateway.iot_device.connect(device)

                rel = device.destination.connect(destination_ip_device,rel_data)
                rel.save()
        else:
            logger.error(f"COULD NOT UPLOAD:{data}")
