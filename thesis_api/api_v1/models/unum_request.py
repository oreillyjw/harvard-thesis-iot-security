from django.db import models


class UnumRequest(models.Model):
    """
    UnumRequest Model
    Defines the attributes of a Unum Request
    """
    type = models.TextField(max_length=255, default="unum")
    data = models.TextField(null=True)
    request_verb = models.TextField(default="Unknown")
    mac_address = models.TextField(default="Unknown")
    uri = models.TextField(max_length=255, default="Unknown")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_uri(self):
        return self.type.title() + ' Type request for uri: ' + self.uri

    def __repr__(self):
        return '{0} request with VERB: {1} for endpoint: {2}'.format(self.type.title(), self.request_verb, self.uri)

    def __str__(self):
        return '{0}'.format(self.type)
