from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo, DateTimeProperty, StructuredRel, ArrayProperty

from neomodel.contrib.spatial_properties import PointProperty

class RequestRel(StructuredRel):
    time = DateTimeProperty(default_now = True)
    bytes_transfer_in = IntegerProperty()
    bytes_transfer_out = IntegerProperty()
    port = IntegerProperty()
    protocol = StringProperty()

class DNSDestination(StructuredNode):
    uid = UniqueIdProperty()
    dns_name = StringProperty(index=True)
    location = PointProperty(crs='wgs-84')

class IPDestination(StructuredNode):
    uid = UniqueIdProperty()
    ip = StringProperty(index=True)
    location = PointProperty(crs='wgs-84')
    dns_names = RelationshipTo(DNSDestination, 'HAS_DNS')

class IOTDevice(StructuredNode):
    uid = UniqueIdProperty()
    mac_address = StringProperty(index=True)
    ip = StringProperty(index=True)
    last_seen = DateTimeProperty(default_now = True)
    # traverse outgoing IS_FROM relations, inflate to Country objects
    destination = RelationshipTo(IPDestination, 'REQUESTED', model=RequestRel )

class GatewayDevice(StructuredNode):
    uid = UniqueIdProperty()
    ip = StringProperty(index=True)
    mac_address = StringProperty(unique_index=True)
    location = PointProperty(crs='wgs-84')

    iot_device = RelationshipTo(IOTDevice, 'CONTAINS')


class NodeGetCreator():
    def get_create(object, data):
        node = object.nodes.first_or_none(**data)
        if node == None:
             node = object.get_or_create(
                data
            )[0]
        return node

    def unum_data_transform(record_data):
        rel_data = {}
        gateway_data = {}
        destination_ip_data = {}
        destination_dns_data = {}
        iot_data = {}
        rel_dict = {
            "b_in" : "bytes_transfer_in",
            "b_out" : "bytes_transfer_out",
            "remote_port" : "port",
            "transfer_protocol" : "protocol"
        }
        destination_ip_dict = {
            "remote_ip" : "ip"
        }
        destination_dns_dict = {
            "remote_dns": "dns_name"
        }
        iot_dict = {
            "local_ip" : "ip",
            "local_mac": "mac_address"
        }
        gateway_dict = {
            "router_mac_address" : "mac_address"
        }
        for key in record_data.keys():
            if record_data[key] is None:
                continue
            if key in rel_dict.keys():
                rel_data[rel_dict[key]] = record_data[key]
            if key in destination_ip_dict.keys():
                destination_ip_data[destination_ip_dict[key]] = record_data[key]
            if key in destination_dns_dict.keys():
                destination_dns_data[destination_dns_dict[key]] = record_data[key]
            if key in iot_dict.keys():
                iot_data[iot_dict[key]] = record_data[key]
            if key in gateway_dict.keys():
                gateway_data[gateway_dict[key]] = record_data[key]

        return rel_data, destination_ip_data, destination_dns_data, iot_data, gateway_data
