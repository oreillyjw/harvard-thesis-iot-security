from django.db import models
from .unum_devices import UnumDevices

class UnumDevicesMeta(models.Model):
    """
    Unum Devices Meta Model
    Defines the attributes of a Unum Device meta data
    """
    router_mac_address = models.TextField(default="Unknown")
    mac_address = models.TextField(default="Unknown")
    type = models.TextField(max_length=20)
    blob=models.TextField(null=True, blank=True)
    name=models.TextField()
    md5=models.TextField()
    port=models.IntegerField()
    device = models.ForeignKey(UnumDevices, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Device Meta Data found for router: {0} has mac address {1} - type: {2} - {3} - {4}'.format(self.router_mac_address, self.mac_address, self.type, self.md5, self.name)
