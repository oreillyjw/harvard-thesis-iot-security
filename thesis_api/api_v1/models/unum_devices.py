from django.db import models

class UnumDevices(models.Model):
    """
    UnumDevices Model
    Defines the attributes of a Unum Devices
    """
    router_mac_address = models.TextField(default="Unknown")
    mac_address = models.TextField(default="Unknown")
    ip = models.TextField(max_length=20)
    details=models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return 'Device found for router: {0} has mac address {1} and ip {2}'.format(self.router_mac_address, self.mac_address, self.ip)

    def __str__(self):
        return 'Device found for router: {0} has mac address {1} and ip {2}'.format(self.router_mac_address, self.mac_address, self.ip)
