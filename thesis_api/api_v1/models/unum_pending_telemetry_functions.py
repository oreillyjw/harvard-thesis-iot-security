from django.db import models
from django.utils.timezone import utc
import datetime

class UnumPendingTelemetryFuncs(models.Model):
    """
    UnumRequest Model
    Defines the attributes of a Unum Request
    """
    mac_address = models.TextField(default="Unknown")
    function = models.TextField(max_length=50)
    processed = models.BooleanField(default=False)
    seq_num = models.IntegerField(default=None, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return 'Pending Telemtry Function: {0} for MAC Address {1}'.format(self.function, self.mac_address)

    def __str__(self):
        return 'Pending Telemtry Function: {0} for MAC Address {1} :: {2}'.format(self.function, self.mac_address, self.updated_at)

    # Diff time is computed in seconds
    def get_from_now(self):
        if self.updated_at:
            now = datetime.datetime.utcnow().replace(tzinfo=utc)
            timediff = now - self.updated_at
            return timediff.total_seconds()
