from django.test import TestCase
from ..models import UnumRequest


class UnunModelTest(TestCase):
    """ Test module for UnumTest model """

    def setUp(self):
        UnumRequest.objects.create(
            type="test",
            request_verb="GET",
            uri="/api/v1/unum/00:00:00:00:00:00",
            mac_address="00:00:00:00:00:00"
        )
        UnumRequest.objects.create(
            type="test",
            request_verb="POST",
            uri="/v3/unums/00:00:00:00:00:00/activate",
            data="{'test':'data'}",
            mac_address="00:00:00:00:00:00"
        )

    def test_select_model(self):
        """Check get objects from model store"""
        get_request = UnumRequest.objects.get(type="test", request_verb="GET", mac_address="00:00:00:00:00:00")
        post_request = UnumRequest.objects.get(type="test", request_verb="POST", mac_address="00:00:00:00:00:00")
        self.assertEqual(
            get_request.get_uri(), "Test Type request for uri: /api/v1/unum/00:00:00:00:00:00")
        self.assertEqual(
            post_request.get_uri(), "Test Type request for uri: /v3/unums/00:00:00:00:00:00/activate")
