import json
import os
from ..data_processor import ProcessorFactory, Processor, UnumProcessor
from django.test import TestCase
from ..models import RequestRel, IPDestination, IOTDevice, GatewayDevice, NodeGetCreator, DNSDestination

CURRENT_DIRECTORY = os.path.dirname(__file__)

class UnumToNeo4jTest(TestCase):
    """Test module for recording all data to Neo4j"""
    def testUnumUploadDevicesTelemtry(self):
        """Test Unum Uploading Device Telemtry endpoint w/data"""
        file = "post-00:00:00:00:00:00-devices-telemetry.json"
        data = self.getFileInfo(file)
        data["source_type"] = "unum"
        factory = ProcessorFactory()
        processor = factory.get_processor(data["source_type"], **data)
        self.assertIsInstance(processor,UnumProcessor)
        self.assertEqual(processor.mac_address, data["mac_address"])
        self.assertEqual(processor.data_type, data["data_type"])
        flat_data = processor.flatten()
        self.assertEqual(len(flat_data), 18)
        for record_data in flat_data:
            rel_data, destination_ip_data, destination_dns_data, iot_data, gateway_data = NodeGetCreator.unum_data_transform(record_data)

            destination_ip_device = NodeGetCreator.get_create(IPDestination, destination_ip_data)
            gateway = NodeGetCreator.get_create(GatewayDevice, gateway_data)
            device = NodeGetCreator.get_create(IOTDevice, iot_data)

            if "dns_name" in destination_dns_data.keys():
                destination_dns_device = NodeGetCreator.get_create(DNSDestination, destination_dns_data)
                if not destination_ip_device.dns_names.is_connected(destination_dns_device):
                    destination_ip_device.dns_names.connect(destination_dns_device)

            if not gateway.iot_device.is_connected(device):
                gate_rel = gateway.iot_device.connect(device)

            rel = device.destination.connect(destination_ip_device,rel_data)
            rel.save()


    def getFileInfo(self, file):
        data = {}
        with open(f"{CURRENT_DIRECTORY}/unum_files/{file}") as json_file:
            data['data'] = json.load(json_file)
            string_data = file.replace(".json","").split("-")
            data['data_type'] = "/".join(string_data[2:])
            data['mac_address'] = string_data[1]
        return data
