import json
from rest_framework import status
from django.test import TestCase, Client, override_settings
from django.urls import reverse
from ..models import UnumRequest
from ..serializers import UnumMacAddressDictSerializer
from django.db.models import Count
import json
import os
from api_v1 import tasks

# initialize the APIClient app
CLIENT = Client()
CURRENT_DIRECTORY = os.path.dirname(__file__)
UNUM_FILES = [
    "post-00:00:00:00:00:00-neighboring_access_points.json",
    "post-00:00:00:00:00:00-radios.json",
    "post-00:00:00:00:00:01-devices-telemetry.json",
    "post-00:00:00:00:00:01-telemetry.json",
    "put-00:00:00:00:00:00-activate.json",
    "put-00:00:00:00:00:00-router_configs-raw.txt"
]



class UnumRecordDataTest(TestCase):
    """ Test module for recording all data via API """

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True,CELERY_TASK_EAGER_PROPOGATES=True)
    def testRecordingData(self):
        """ Test each file in the unum files directory for correct recordings"""
        for file in UNUM_FILES:
            with open(f"{CURRENT_DIRECTORY}/unum_files/{file}") as json_file:
                extension = file.split(".")
                if extension[1] == "json":
                    data = json.load(json_file)
                    content_type = "application/json"
                else:
                    data = json_file
                    content_type = "text/plain"

                #TODO Add split for extension of file
                string_data = extension[0].split("-")
                url_action = "/".join(string_data[2:])
                reqeust_type = string_data[0].upper()
                mac_address= string_data[1]
                content_type = "application/json" if extension[1] == "json" else  "text/plain"
                if reqeust_type == "POST":
                    response = CLIENT.post(
                        reverse('store_unum_request', kwargs={"mac_address":mac_address, "uri_info":url_action}),
                        data,
                        content_type=content_type
                    )
                elif reqeust_type == "PUT":
                    response = CLIENT.put(
                        reverse('store_unum_request', kwargs={"mac_address":mac_address, "uri_info":url_action}),
                        data,
                        content_type=content_type
                    )
                else:
                    raise Exception(f"Unable to test request with method: {request_type}")

                self.assertEqual(response.status_code, status.HTTP_200_OK)


    def testconfirmRecordedData(self):
        """ Confirm recorded data """
        # Add recording from previous test
        self.testRecordingData()
        requests = UnumRequest.objects.values('mac_address').annotate(the_count=Count('mac_address'))
        serializer = UnumMacAddressDictSerializer(requests, many=True)
        # get API response
        response = CLIENT.get(reverse('get_all_unum_request'))
        self.assertEqual(len(response.json()), 2)
        mapped = {x["mac_address"]: x["count"] for x in response.json()}
        self.assertEqual(mapped["00:00:00:00:00:00"], 4)
        self.assertEqual(mapped["00:00:00:00:00:01"], 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
