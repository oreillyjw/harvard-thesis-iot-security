import json
import os
from ..data_processor import ProcessorFactory, Processor, UnumProcessor
from django.test import TestCase

CURRENT_DIRECTORY = os.path.dirname(__file__)

class ProcessorFactoryTest(TestCase):
    """Test module for recording all data via API """

    def testEmptyProcessor(self):
        """Default Processor """
        factory = ProcessorFactory()
        processor = factory.get_processor()
        self.assertIsInstance(processor,Processor)

    def testUnumProcessor(self):
        """Unum Processor """
        factory = ProcessorFactory()
        processor = factory.get_processor("unum")
        self.assertIsInstance(processor,UnumProcessor)

    def testProtocolNumberToName(self):
        """Test converstion of Internet Number standard to name"""
        factory = ProcessorFactory()
        processor = factory.get_processor()
        self.assertIsInstance(processor,Processor)
        self.assertEqual(processor.transfer_protocol_name(6), "tcp")

    def testUnumProcessorDevicesTelemtry(self):
        """Test Unum Processor for Device Telemtry endpoint w/data"""
        file = "post-00:00:00:00:00:00-devices-telemetry.json"
        data = self.getFileInfo(file)
        data["source_type"] = "unum"
        factory = ProcessorFactory()
        processor = factory.get_processor(data["source_type"], **data)
        self.assertIsInstance(processor,UnumProcessor)
        self.assertEqual(processor.mac_address, data["mac_address"])
        self.assertEqual(processor.data_type, data["data_type"])
        flat_data = processor.flatten()
        self.assertEqual(len(flat_data), 18)

    def testUnumProcessorDevicesTelemtryNoData(self):
        """Test Unum Processor for Device Telemtry endpoint w/ no data"""
        file = "post-00:00:00:00:00:01-devices-telemetry.json"
        data = self.getFileInfo(file)
        data["source_type"] = "unum"
        factory = ProcessorFactory()
        processor = factory.get_processor(data["source_type"], **data)
        self.assertIsInstance(processor,UnumProcessor)
        self.assertEqual(processor.mac_address, data["mac_address"])
        self.assertEqual(processor.data_type, data["data_type"])
        flat_data = processor.flatten()
        self.assertEqual(len(flat_data), 0)

    def testUnumProcessorRadioNoData(self):
        """Test Unum Processor for Radio endpoint"""
        file = "post-00:00:00:00:00:00-radios.json"
        data = self.getFileInfo(file)
        data["source_type"] = "unum"
        factory = ProcessorFactory()
        processor = factory.get_processor(data["source_type"], **data)
        self.assertIsInstance(processor,UnumProcessor)
        self.assertEqual(processor.mac_address, data["mac_address"])
        self.assertEqual(processor.data_type, data["data_type"])
        flat_data = processor.flatten()
        self.assertEqual(len(flat_data), 0)

    def testUnumProcessorDevicesTelemtryAndFingerprint(self):
        """Test Unum Processor for Device Telemtry endpoint device and fingerprint data"""
        file = "post-00:00:00:00:00:02-devices-telemetry.json"
        data = self.getFileInfo(file)
        data["source_type"] = "unum"
        factory = ProcessorFactory()
        processor = factory.get_processor(data["source_type"], **data)
        self.assertIsInstance(processor,UnumProcessor)
        self.assertEqual(processor.mac_address, data["mac_address"])
        self.assertEqual(processor.data_type, data["data_type"])
        flat_data = processor.flatten()
        self.assertEqual(len(flat_data), 12)
        fingerprint_data = processor.flatten_fingerprints()
        self.assertEqual(len(fingerprint_data), 4)

    def getFileInfo(self, file):
        data = {}
        with open(f"{CURRENT_DIRECTORY}/unum_files/{file}") as json_file:
            data['data'] = json.load(json_file)
            string_data = file.replace(".json","").split("-")
            data['data_type'] = "/".join(string_data[2:])
            data['mac_address'] = string_data[1]
        return data
