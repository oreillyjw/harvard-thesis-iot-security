import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import UnumRequest
from ..serializers import UnumMacAddressDictSerializer
from django.db.models import Count
import json
import os

# initialize the APIClient app
CLIENT = Client()
CURRENT_DIRECTORY = os.path.dirname(__file__)
UNUM_FILES = [
    "post-00:00:00:00:00:00-neighboring_access_points.json",
    "post-00:00:00:00:00:00-radios.json",
    "post-00:00:00:00:00:01-devices-telemetry.json",
    "post-00:00:00:00:00:01-telemetry.json",
    "put-00:00:00:00:00:00-activate.json",
    "post-00:00:00:00:00:00-devices-telemetry.json"
]


class UnumGetMacAddressesTest(TestCase):
    """ Test module for GET all MacAddresses API """

    def setUp(self):

        for file in UNUM_FILES:
            with open(f"{CURRENT_DIRECTORY}/unum_files/{file}") as json_file:
                data = json.load(json_file)

            string_data = file.replace(".json","").split("-")
            url_action = "/".join(string_data[2:])
            UnumRequest.objects.create(
                request_verb=string_data[0].upper(),
                uri=f"/v3/unums/{string_data[1]}/{url_action}",
                data=str(data),
                mac_address=string_data[1]
            )

    def test_get_all_mac_addresses(self):
        """Confirm Mac Address Serializer """
        # get data from db
        requests = UnumRequest.objects.values('mac_address').annotate(the_count=Count('mac_address'))
        serializer = UnumMacAddressDictSerializer(requests, many=True)
        # get API response
        response = CLIENT.get(reverse('get_all_unum_request'))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
