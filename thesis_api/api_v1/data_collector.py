from django.db import models


class DataCollector(models.Model):
    """
    Data Collector Model
    Defines the attributes of a puppy
    """
    data = models.CharField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_breed(self):
        return self.name + ' belongs to ' + self.breed + ' breed.'

    def __repr__(self):
        return self.name + ' is added.'

    class Meta:
     db_table = "data_storage"
