# Generated by Django 2.2.4 on 2019-09-03 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_v1', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unumrequest',
            name='type',
            field=models.TextField(default='unum', max_length=255),
        ),
        migrations.AlterField(
            model_name='unumrequest',
            name='uri',
            field=models.TextField(max_length=255),
        ),
    ]
