import os,sys, django
import time

from neomodel import db
import logging.config
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        }
    },
    'loggers': {
        'neobolt':{
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        '': {
            'level': "INFO",
            'handlers': ['console'],
        },
    },
})

def run():
        # MATCH (iot1:IOTDevice {mac_address: $mac_address})-[r1:REQUESTED]->(dest:IPDestination)

    query = """
MATCH (iot1:IOTDevice)-[r1:REQUESTED]->(dest1:IPDestination)
WHERE r1.time >= $start_date AND r1.time <= $end_date
WITH iot1, collect(id(dest1)) AS iot1Dest1
MATCH (iot2:IOTDevice)-[r2:REQUESTED]->(dest2:IPDestination)
WHERE r2.time <= $end_date
WITH iot1, iot1Dest1, iot2, collect(id(dest2)) AS iot2Dest2
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.jaccard(iot1Dest1, iot2Dest2) AS similarity
    """
    os.environ["TZ"] = "US/Eastern"
    start_date='2019-12-08 16:00:00'
    end_date='2019-12-09 00:00:00'
    p='%Y-%m-%d %H:%M:%S'
    start_epoch_span = int(time.mktime(time.strptime(start_date,p)))
    process_date = start_epoch_span
    end_epoch_span = int(time.mktime(time.strptime(end_date,p)))
    time_span = 60
    while process_date < end_epoch_span:
        print(f"{process_date} - {process_date+time_span}")
        params = {
            # "mac_address" : "dc:a6:32:09:8b:c8",
            "start_date"  : process_date,
            "end_date"    : process_date + time_span
        }
        results, meta = db.cypher_query(query, params)
        print(results)
        process_date = process_date+time_span
