import json
from api_v1.models import ElasticsearchBackend


def cosine_pearson(dates, info, array):
   similarity_avg_in = 0
   similarity_avg_out = 0
   if array[2] != 0:
       similarity_avg_out = array[3]/array[2]

   if array[3] != 0:
       similarity_avg_in = array[2]/array[3]

   dict = {
        "to": array[0],
        "from": array[1],
        "similarity_out": array[2],
        "similarity_in": array[3],
        "similarity_avg_in": similarity_avg_in,
        "similarity_avg_out": similarity_avg_out,
        "similarity_avg": (array[2] + array[3])/2,
        "start_time": dates[0],
        "end_time": dates[1],
        "similarity_type": info['similarity_type']
   }

   return dict

def nearest_neighbor(dates, info, array):
   dict = {
        "to": array[0],
        "from": array[1],
        "similarity_avg": array[2],
        "start_time": dates[0],
        "end_time": dates[1],
        "similarity_type": info['similarity_type']
   }

   return dict


def run(*args):
    cosine_file = {
        # "filename": ["1min_20191208/cosine_similarity_output_20191209000000_20191209120000.log"],
        "filename" : ["1min_20191214/cosine.similarity.log"],
        "similarity_type" : "cosine",
        "index": "similarity"
    }

    pearson_file = {
        # "filename": ["1min_20191208/pearson_similarity_20191208160000_20191209020000.log"],
        "filename": ["1min_20191214/pearson.similarity.log"],
        "similarity_type" : "pearson",
        "index": "similarity"
    }

    nearest_neighbor_file = {
        # "filename": ["1min/nearest_similarity_20191208160000_20191209020000.log"],
        "filename": ["1min_20191214/nearest.similarity.log"],
        "similarity_type" : "nearest_neighbor",
        "index": "similarity"
    }

    jaccard_file = {
        # "filename": ["1min/nearest_similarity_20191208160000_20191209020000.log"],
        "filename": ["1min_20191214/jaccard.similarity.log"],
        "similarity_type" : "jaccard",
        "index": "similarity"
    }
    
    if "pearson" in args:
        info = pearson_file
    elif "cosine" in args:
        info = cosine_file
    elif "nearest_neighbor" in args:
        info = nearest_neighbor_file
    elif "jaccard" in args:
        info = jaccard_file

    print(info.keys())
    #Open the file back and read the contents
    files = info['filename']
    for filename in files:
        f=open(f"scripts/data/{filename}", "r")
        # or, readlines reads the individual line into a list
        fl = f.readlines()
        prevLine = ""
        count = 0
        for line in fl:
           count+=1
           if count % 2 == 0:
               # print(line)
               arrays = eval(line)
               dates = list(map(int, prevLine.strip().split(" - ")))
               es = ElasticsearchBackend(index=info['index'])
               for array in arrays:
                   if len(array) == 4:
                       dict = cosine_pearson(dates, info, array)
                   elif len(array) == 3:
                       dict = nearest_neighbor(dates, info, array)

                   es.set(dict)
           else:
               prevLine = line
