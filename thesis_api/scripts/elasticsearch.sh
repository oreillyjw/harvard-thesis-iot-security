PUT _template/similarity
{
  "index_patterns": ["similarity*"],
  "settings": {
    "number_of_shards": 1
  },
  "aliases" : {
    "pearson" : {
      "filter" : {
           "term" : {"similarity_type" : "pearson" }
       }
    },
    "cosine" : {
      "filter" : {
           "term" : {"similarity_type" : "cosine" }
       }
    },
    "nearest_neighbor" : {
      "filter" : {
           "term" : {"similarity_type" : "nearest_neighbor" }
       }
    },
    "jaccard" : {
      "filter" : {
           "term" : {"similarity_type" : "jaccard" }
       }
    }
  },
  "mappings": {
    "_source": {
      "enabled": true
    },
    "properties": {
      "to" : {
        "type": "text",
         "fields": {
           "keyword": {
             "type": "keyword",
             "ignore_above": 256
           }
       }
      },
      "from" : {
        "type": "text",
         "fields": {
           "keyword": {
             "type": "keyword",
             "ignore_above": 256
           }
         }
      },
      "similarity_out" : {
        "type": "float"
      },
      "similarity_in" : {
        "type": "float"
      },
      "similarity_avg_in" : {
        "type": "float"
      },
      "similarity_avg_out" : {
        "type": "float"
      },
      "similarity_avg" : {
        "type": "float"
      },
      "start_time": {
        "type": "date",
        "format": "epoch_second"
      },
      "end_time": {
        "type": "date",
        "format": "epoch_second"
      }
    }
  }
}
