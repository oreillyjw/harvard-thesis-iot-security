# Neo4j Queries

## Basic
* Last hour of requests
```sql
WITH (timestamp() - (1000*60*60*1))/1000 AS last_time
MATCH (iot:IOTDevice)-[req:REQUESTED]->(ip:IPDestination)
WHERE toInteger(req.time) >= last_time
RETURN iot,req,ip
```

* 5min of requests
```sql
WITH .083 AS time_diff
WITH (timestamp() - (1000*60*60*time_diff))/1000 AS last_time
MATCH (iot:IOTDevice)-[req:REQUESTED]->(ip:IPDestination)
WHERE toInteger(req.time) >= last_time
RETURN *
```

* IP Destination of `23.78.208.56` that has DNS Destination
```sql
MATCH (ip:IPDestination)-[has_dns:HAS_DNS]-(dns:DNSDestination)
WHERE ip.ip = "23.78.208.56"
RETURN *
```

* Attacked IP
```sql
MATCH (iot:IOTDevice)-[r:REQUESTED]-(n:IPDestination)-[hd:HAS_DNS]->(dns:DNSDestination)
where n.ip ='3.91.228.51'
RETURN iot,r,n,hd,dns LIMIT 25
```

* All UDP Requests between December 8, 2019 8:50:07 PM AND December 8, 2019 9:40:04 PM GMT-05:00
```
MATCH (iot:IOTDevice)-[r:REQUESTED]-(n:IPDestination)-[hd:HAS_DNS]->(dns:DNSDestination)
where r.time > 1575856207 AND r.time < 1575859204 AND r.protocol = "udp"
RETURN iot,r,n,hd,dns
```

* Delete All
```
MATCH (n)
DETACH DELETE n
```

* Install algorithms:
  - https://github.com/neo4j-contrib/neo4j-graph-algorithms
    - Pearson Score
    - Cosine Similarity
