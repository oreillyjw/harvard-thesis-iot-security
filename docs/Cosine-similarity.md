* COSINE Similarity Algorithm
* All Requests
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
MATCH (iot2:IOTDevice )-[r2:REQUESTED]->(dest:IPDestination) WHERE iot2 <> iot1
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.cosine(collect(r1.bytes_transfer_out), collect(r2.bytes_transfer_out)) AS similarity_out,
       algo.similarity.cosine(collect(r1.bytes_transfer_in), collect(r2.bytes_transfer_in)) AS similarity_in
ORDER BY similarity DESC
```

* NOT UDP
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
WHERE NOT r1.protocol = "udp"
MATCH (iot2:IOTDevice )-[r2:REQUESTED]->(dest:IPDestination) WHERE iot2 <> iot1 AND NOT r2.protocol = "udp"
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.cosine(collect(r1.bytes_transfer_out), collect(r2.bytes_transfer_out)) AS similarity_out,
       algo.similarity.cosine(collect(r1.bytes_transfer_in), collect(r2.bytes_transfer_in)) AS similarity_in
ORDER BY similarity_out DESC
```

* NOT TCP
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
WHERE NOT r1.protocol = "tcp"
MATCH (iot2:IOTDevice )-[r2:REQUESTED]->(dest:IPDestination) WHERE iot2 <> iot1 AND NOT r2.protocol = "tcp"
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.cosine(collect(r1.bytes_transfer_out), collect(r2.bytes_transfer_out)) AS similarity_out,
       algo.similarity.cosine(collect(r1.bytes_transfer_in), collect(r2.bytes_transfer_in)) AS similarity_in
ORDER BY similarity_out DESC
```

* TCP
```
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
WHERE r1.protocol = "tcp"
MATCH (iot2:IOTDevice )-[r2:REQUESTED]->(dest:IPDestination) WHERE iot2 <> iot1 AND r2.protocol = "tcp"
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.cosine(collect(r1.bytes_transfer_out), collect(r2.bytes_transfer_out)) AS similarity_out,
       algo.similarity.cosine(collect(r1.bytes_transfer_in), collect(r2.bytes_transfer_in)) AS similarity_in
ORDER BY similarity_out DESC
```
