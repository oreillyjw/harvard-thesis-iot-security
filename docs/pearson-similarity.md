* Pearson Similarity Score

* All Requests
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
MATCH (iot2:IOTDevice )-[r2:REQUESTED]->(dest:IPDestination) WHERE iot2 <> iot1
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.cosine(collect(r1.bytes_transfer_out), collect(r2.bytes_transfer_out)) AS similarity_out,
       algo.similarity.cosine(collect(r1.bytes_transfer_in), collect(r2.bytes_transfer_in)) AS similarity_in
ORDER BY similarity_out DESC
```

* Time Range and TCP Requests
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
WHERE r1.time > 1575856207 AND r1.time < 1575859204 AND r1.protocol = "tcp"
WITH iot1, algo.similarity.asVector(dest, r1.bytes_transfer_out) AS iotout1Vector,
algo.similarity.asVector(dest, r1.bytes_transfer_in) AS iotin1Vector
MATCH (iot2:IOTDevice)-[r2:REQUESTED]->(dest:IPDestination)
WHERE r2.time < 1575859204 AND r2.protocol = "tcp"
WITH iot1, iot2, iotout1Vector, iotin1Vector, algo.similarity.asVector(dest, r2.bytes_transfer_out) AS iotout2Vector,
algo.similarity.asVector(dest, r2.bytes_transfer_in) AS iotin2Vector
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.pearson(iotout1Vector, iotout2Vector, {vectorType: "maps"}) AS similarity_out,
       algo.similarity.pearson(iotin1Vector, iotin2Vector, {vectorType: "maps"}) AS similarity_in
```

* Time Range and Not UDP
```sql
MATCH (iot1:IOTDevice {mac_address: "dc:a6:32:09:8b:c8"})-[r1:REQUESTED]->(dest:IPDestination)
WHERE r1.time > 1575856207 AND r1.time < 1575859204 AND NOT r1.protocol = "udp"
WITH iot1, algo.similarity.asVector(dest, r1.bytes_transfer_out) AS iotout1Vector,
algo.similarity.asVector(dest, r1.bytes_transfer_in) AS iotin1Vector
MATCH (iot2:IOTDevice)-[r2:REQUESTED]->(dest:IPDestination)
WHERE r2.time > 1575856207 AND r2.time < 1575859204 AND NOT r2.protocol = "udp"
WITH iot1, iot2, iotout1Vector, iotin1Vector, algo.similarity.asVector(dest, r2.bytes_transfer_out) AS iotout2Vector,
algo.similarity.asVector(dest, r2.bytes_transfer_in) AS iotin2Vector
RETURN iot1.human_name AS from,
       iot2.human_name AS to,
       algo.similarity.pearson(iotout1Vector, iotout2Vector, {vectorType: "maps"}) AS similarity_out,
       algo.similarity.pearson(iotin1Vector, iotin2Vector, {vectorType: "maps"}) AS similarity_in
```       
