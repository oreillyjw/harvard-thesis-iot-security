- Overlap
  * no dice
```sql
MATCH (iot:IOTDevice )-[r:REQUESTED]->(dest:IPDestination)
WHERE r.time > 1575856207 AND r.time < 1575859204
WITH {item:id(dest), categories: collect(id(iot))} as userData
WITH collect(userData) as data
CALL algo.similarity.overlap.stream(data)
YIELD item1, item2, count1, count2, intersection, similarity
RETURN algo.asNode(item1).human_name AS from, algo.asNode(item2).human_name AS to,
     count1, count2, intersection, similarity
ORDER BY similarity DESC
```
